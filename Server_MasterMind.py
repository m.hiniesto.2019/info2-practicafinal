import threading
from socket import *
import sys
import getopt
from HiloMasterMind import Client


def conseguir_argumentos():
    ip = '0.0.0.0'
    port = 1234
    opts, args = getopt.getopt(sys.argv[1:], 'i:p:', ['ip=', 'port='])
    for o, a in opts:
        if o in ('-i', '--ip'):
            ip = a
        elif o in ('-p', '--port'):
            port = a
    return ip, port


def comprobar_argumentos(ip, port):
    port_ok = True
    ip_ok = True
    try:
        port = int(port)
    except ValueError:
        port_ok = False

    if len(ip) == 0:
        ip_ok = False
    return ip_ok, port_ok


class TCPserver():
    def __init__(self, serverHost, serverPort):
        self.serverHost = serverHost
        self.serverPort = serverPort
        self.serverSocket = None

    def crear(self):
        self.serverSocket = socket(AF_INET, SOCK_STREAM)
        self.serverSocket.bind((self.serverHost, self.serverPort))
        self.serverSocket.listen(1)
        print("El servidor esta listo para ser usado")

    def cerrar(self):
        self.serverSocket.close()
        self.serverSocket = None

    def getSocket(self):
        return self.serverSocket

    def comunicacion(self):
        while True:
            clientsocket, direccion = self.serverSocket.accept()
            print("conexion desde: ", direccion)
            c = Client(ip, port, clientsocket)
            c.start()
            print(f'[Active connections] {threading.active_count() -1}')


def main(ip, port):
    servidor = TCPserver(ip, port)
    servidor.crear()
    servidor.comunicacion()
    servidor.cerrar()


if __name__ == '__main__':

    ip, port = conseguir_argumentos()
    ip_ok, port_ok = comprobar_argumentos(ip, port)
    if not ip_ok:
        print('Error en la ejecucion de la aplicacion.  Debe usar el parametro -i o --ip= con su direccion ip')
    if not port_ok:
        print('Error en la ejecucion de la aplicacion. Debe usar el parametro -p o --port= con su puerto')

    main(ip, int(port))






