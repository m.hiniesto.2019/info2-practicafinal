Práctica relizada por:
Mario Hiniesto Iñigo
Víctor Hidalgo Ortega
Guillermo Andrés Castro Abarca

No hemos llegado a finalizar la parte de listas enlazadas pero el resto está completo e implementado.
Mientras realizábamos la práctica se nos han quedado colgados los códigos de vez en cuando debido a la inesperada caida
de VNCweb y Pycharm. Si os ocurre a vosotros simplemente hay que volver a ejecutar los ficheros.