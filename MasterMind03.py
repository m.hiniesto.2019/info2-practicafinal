import random

# esqueleto inicial
class MasterMindGame:
    # declaramos las variables que vamos a utiliza

    MMC = {}

    secretCode = []  # código secreto que tenemos que adivinar.

    validColors = "rgybkw"  # colores mastermind permitidos

    currentTurn = 0  # turno actual.

    maxTurn = 10 # máximo número de turnos para acertar la clave.


    # construimos la función para iniciar la clase
    def __init__(self, combiCode: str = "nocombiCode"):   # iniciamos el diccionario de colores

        self.MMC["red"] = "🔴"
        self.MMC["green"] = "🟢"
        self.MMC["yellow"] = "🟡"
        self.MMC["blue"] = "🔵"
        self.MMC["black"] = "⚫"
        self.MMC["white"] = "⚪"

        # máximo número de turnos para acertar la clave.

        if combiCode == "nocombiCode":
            self.secretCode = self.randomCode(4)
        else:
            try:
                self.secretCode = self.toMasterMindColorCombination(list(combiCode))
            except:
                self.secretCode = self.randomCode(4)


    def randomCode(self, n: int) -> list:  # genera un código aleatorio

        colorList = list(self.validColors)
        passCode = random.choices(colorList, k=n)
        return self.toMasterMindColorCombination(passCode)

    def MasterMindColor(self, color: str):  # convertir cadenas en colores

        rcolor = "Color no encontrado"

        if color == "R" or color == "r" or color == "🔴":
            rcolor = self.MMC["red"]
        elif color == "G" or color == "g" or color == "🟢":
            rcolor = self.MMC["green"]
        elif color == "Y" or color == "y" or color == "🟡":
            rcolor = self.MMC["yellow"]
        elif color == "B" or color == "b" or color == "🔵":
            rcolor = self.MMC["blue"]
        elif color == "K" or color == "k" or color == "⚫":
            rcolor = self.MMC["black"]
        elif color == "W" or color == "w" or color == "⚪":
            rcolor = self.MMC["white"]
        else:
            raise KeyError(rcolor)

        return rcolor

    def toMasterMindColorCombination(self, combi: list) -> list:  # obtener una cadena de colores mastermind
        return list(map(lambda n: self.MasterMindColor(n), combi))

    def countExactMatches(self, mmCombi):

        compResult = list(map(lambda x, y: x == y, mmCombi, self.secretCode))

        return len(list(filter(lambda x: x == True, compResult)))



    #comparar la combinacion del jugador con secret code

    def countPartialMatches(self, mmCombi: list) -> int:

        csMatches = 0

        tSecret = tuple(map(lambda x, y: (x, x == y), self.secretCode, mmCombi))

        lGuess = list(map(lambda x, y: (y, x == y), self.secretCode, mmCombi))


        def chkMatch(x)-> tuple:
            isChecked = False

            if lGuess.__contains__(x) and not x[1]:
                index = lGuess.index(x)
                if tSecret[index][1] == False:
                    lGuess[index] = (x[0], True)
                    isChecked = True

            return (x[0], isChecked)

        tsMatches = tuple(map(lambda x: chkMatch(x), tSecret))

        for x in tsMatches:
            if x[1] == True:
                csMatches += 1

        return csMatches

    def newTurn(self, guess: str):
        pGuess = []
        bGuessMatch = 0
        try:
            pGuess = self.toMasterMindColorCombination(guess)
        except:
            return "Combinacion Incorrecta. Porfavor, pruebe de nuevo."

        #comparar el numero de tiradas <= 10

        lSecretCode = len(self.secretCode)
        if len(pGuess) != lSecretCode:
            msg = "{} No es la longitud adecuada: "
            return msg.format(pGuess)

        if self.currentTurn == self.maxTurn or bGuessMatch:
            return 'El juego ha terminado.'


        self.currentTurn += 1
        nMatches = self.countExactMatches(pGuess)
        sMatches = self.countPartialMatches(pGuess)


        if lSecretCode == nMatches:

            msg = f'Has ganado en el turno {self.currentTurn}'

            self.currentTurn = self.maxTurn
            return msg
        else:
            resultado = "Tu Combinacion: {}, Aciertos: {}, SemiAciertos: {}"
            return resultado.format(pGuess, nMatches, sMatches)
