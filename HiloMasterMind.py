import random
import sys
from threading import Thread
from MasterMind03 import MasterMindGame
import time

lista_sockets = {}
partidas = {}

class faseJuego(MasterMindGame):

    def __init__(self, combiCode: str = 'nocomibiCode'):
        MasterMindGame.__init__(self)
        self.mm = MasterMindGame(combiCode)

    def newturn(self,guess):
        return self.mm.newTurn(guess)

    def maxTurn(self, Turns: int):
        self.mm.maxTurn = Turns
def juegopartida(jugador1, jugador2):
    configuracion = 'CONFI#Mensaje'
    espera = 'ESPERA# Partida iniciada, espera a que el jugador 1 configure la partida...'
    jugador1.send(configuracion.encode())
    jugador2.send(espera.encode())
    confipartida = jugador1.recv(1024).decode()
    data = confipartida.split('#')
    turnos = int(data[1])
    game = faseJuego(data[2])
    game.maxTurn(turnos)
    
    msgg = f"INFO#Tu partida se ha configurado correctamente a {data[1]} turnos"
    jugador1.send(msgg.encode())
    empezar = f'JUGAR#{turnos}'
    jugador2.send(empezar.encode())
    for v in range(turnos):
        data = jugador2.recv(1024).decode()
        infocliente1 = f'INFO#Esta ha sido el intendo de el jugador 2: {data}'
        data2 = game.newturn(data)
        if data2 == f"Hasganadoenelturno":
            jugador2.send(data2.encode())
            ganador = f'INFO#Ha gandado el jugador 2 en el turno: {data2.split()[5]}'
            jugador1.send(ganador.encode())
            break
        else:
            jugador1.send(infocliente1.encode())
            jugador2.send(data2.encode())

class Client(Thread):
    def __init__(self, ip, port, socket):
        Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.socket = socket


    def run(self):

        connected = True

        while connected:
            data = self.socket.recv(1024).decode()
            data = data.split('#')
            if data[0] == "CREAR":
                partidas[data[1]] = 1
                lista_sockets[data[1]] = {
                    'partida_id': random.randint(0, 500),
                    'cliente1': self.socket
                }
                print(partidas)
                msj = "Tu partida se ha creado"
                self.socket.send(msj.encode())
            elif data[0] == "UNIR":
                print(partidas)
                newDict = {}
                newlist = []
                for (keys, values) in lista_sockets.items():
                    newlist.append(values['partida_id'])
                for (key, value) in partidas.items():
                    if value < 2:
                        newDict[key] = value
                    else:
                        pass
                partidas_a_elegir = '#'
                lista_sockets.keys()
                for v in list(newDict.keys()):
                    partidas_a_elegir = partidas_a_elegir + f'{v}&'
                partidas_a_elegir = partidas_a_elegir + '#'
                for v in newlist:
                    partidas_a_elegir = partidas_a_elegir + f'id de la partida: {v}&'
                partidas_a_elegir = partidas_a_elegir + '#'
                partidas_disponibles = f'{partidas_a_elegir}'
                self.socket.send(partidas_disponibles.encode())
                partida_elegida = self.socket.recv(1024).decode()
                partidas[partida_elegida] = 2
                lista_sockets[partida_elegida]['cliente2'] = self.socket
                print(partidas)
                print(lista_sockets)
                socket1 = lista_sockets[partida_elegida]['cliente1']
                socket2 = lista_sockets[partida_elegida]['cliente2']
                juegopartida(socket1, socket2)
                juegopartida(socket2, socket1)
                

#lista_sockets.send(msg.encode("utf-8"))