from socket import *
import getopt
import sys


def conseguir_argumentos():
    ip = '0.0.0.0'
    port = 1234
    name = 'John Doe'
    opts, args = getopt.getopt(sys.argv[1:], 'i:p:n:', ['ip=', 'port=', 'name='])
    for o, a in opts:
        if o in ('-i', '--ip'):
            ip = a
        elif o in ('-p', '--port'):
            port = a
        elif o in ('-n', '--name'):
            name = a
    return ip, port, name


def comprobar_argumentos(ip, port, name):
    port_ok = True
    ip_ok = True
    name_ok = True
    try:
        port = int(port)
    except ValueError:
        port_ok = False

    if len(ip) == 0:
        ip_ok = False

    if len(name) == 0:
        name_ok = False

    return ip_ok, port_ok, name_ok


class TCPclient:
    def __init__(self, serverName, serverPort):
        self.serverName = serverName
        self.serverPort = serverPort
        self.clientSocket = None

    def conexion(self):
        self.clientSocket = socket(AF_INET, SOCK_STREAM)
        self.clientSocket.connect((self.serverName, self.serverPort))

    def enviar(self, mensaje):
        self.clientSocket.send(mensaje.encode())

    def recibir(self):
        respuesta = self.clientSocket.recv(1024).decode()
        return respuesta

    def cerrar(self):
        self.clientSocket.close()
        self.clientSocket = None

    def comunicacion(self, name):
        while True:
            if self.clientSocket is None:
                self.conexion()
            escoger = input("1. Crear partida\r\n2. Unirse a partida\r\n3. Salir\r\n")
            if escoger == "1":
                nombre_partida = input("Introduzca el nombre de su partida: ")
                msg = f"CREAR#{nombre_partida}"
                self.enviar(msg)
                print(self.recibir())
                print('Esperando a que se una otro jugador...')
            elif escoger == "2":
                msg = "UNIR#"
                self.enviar(msg)
                self.enviar(msg)
                print('Estas son las partidas disponibles:\r\n')
                lista = list(self.recibir().split('#'))[1:-1]
                nombres = list(lista[0].split('&')[0:-1])
                ids = list(lista[1].split('&')[0:-1])
                nuevalista = []
                for i in range(len(nombres)):
                    nuevalista.append((nombres[i], ids[i]))
                contador = 1
                for n, i in nuevalista:
                    print(f'{contador}.- {n} {i}\r\n')
                    contador = contador + 1
                eleccion = int(input('Escriba el numero de la partida: '))
                eleccion = eleccion - 1
                msj = f'{nombres[eleccion]}'
                self.enviar(msj)
            elif escoger == "3":
                sys.exit("¡¡¡Hasta la próxima!!!")

            while True:

                comienzo = self.recibir()
                protocol = comienzo.split('#')[0]
                mensaje = comienzo.split('#')[1]
                if protocol == 'CONFI':
                    primera_fase = True
                    combi = True
                    turn = True
                    while turn == True:
                        global turnos
                        turnos = input(
                            f'-Partida de {name}: \n Introduzca el número de turnos que desea para su partida: ')
                        try:
                            int(turnos)
                            turn = False
                        except ValueError:
                            print("Los turnos deben ser números enteros")

                    while combi == True:
                        global CombinacionSecreta
                        CombinacionSecreta = input(f'-Partida de {name}: \n Introduzca una comb'
                                                   f'inacion secreta valida, en caso\n de querer aleatoriedad introduzca noCombiCode:')
                        if len(CombinacionSecreta) != 4:
                            print("La combinación debe ser de 4 colores o noCombiCode.")
                        elif CombinacionSecreta == "noCombiCode":
                            combi = False
                        else:
                            combi = False

                    while primera_fase == True:
                        msg = f"TURN#{turnos}#{str(CombinacionSecreta)}"
                        self.enviar(msg)
                        respuesta = self.recibir()
                        print(respuesta)
                        primera_fase = False
                elif protocol == 'ESPERA':
                    print(mensaje)
                elif protocol == 'INFO':
                    print(mensaje)
                elif protocol == 'JUGAR':
                    turno = mensaje
                    for a in range(int(turno)):
                        guess = input(f"Introduzca combinación: ")
                        self.enviar(guess)
                        respuesta = self.recibir()
                        respuesta2 = respuesta.split()[0] + respuesta.split()[1] + respuesta.split()[2] + \
                                     respuesta.split()[3] + respuesta.split()[4]
                        if respuesta2 == f"Hasganadoenelturno":
                            print(respuesta)
                            break
                        else:
                            print(respuesta)


def main(ip, port, name):
    cliente = TCPclient(ip, port)
    cliente.comunicacion(name)
    cliente.cerrar()


if __name__ == '__main__':
    ip, port, name = conseguir_argumentos()
    ip_ok, port_ok, name_ok = comprobar_argumentos(ip, port, name)
    if not ip_ok:
        print('Error en la ejecucion de la aplicacion.  Debe usar el parametro -i o --ip= con su direccion ip')
    if not port_ok:
        print('Error en la ejecucion de la aplicacion. Debe usar el parametro -p o --port= con su puerto')

    main(ip, int(port), name)

# hello there

